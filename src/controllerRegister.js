import Paciente from './paciente';
import Cuenta from './cuenta';
import Doctor from './doctor';
import Horario from './horario';
import swal from 'sweetalert';

export default class ControllerRegister{

	createPatient(){

		let name = document.getElementById("inputName").value;
		let lastName = document.getElementById("inputLastName").value;
		let gender = document.getElementById("inputGender").value;
		let dni = document.getElementById("inputDni").value;
		let date = document.getElementById("inputDate").value;
		let city = document.getElementById("inputCity").value;
		let medicalInsurance = document.getElementById("inputMedicalInsurance").value;//obra social
		let id = JSON.parse(localStorage.getItem('accountsPatients')) || []
		id = "p" + id.length;
		let tipo="Patient";
		let patient = new Paciente(name,lastName,dni,date,gender,city,medicalInsurance);
		this.createCuenta(patient, id,tipo);
	}

	createDoctor(doctor){

		let name = document.getElementById("inputName").value;
		let lastName = document.getElementById("inputLastName").value;
		let gender = document.getElementById("inputGender").value;
		let dni = document.getElementById("inputDni").value;
		let date = document.getElementById("inputDate").value;
		let city = document.getElementById("inputCity").value;
		let speciality = document.getElementById("inputSpeciality").value;
		let licence = document.getElementById("inputLicenceNumber");
		let schedule = this.createScheduleDoctor();
		let d = new Doctor(name,lastName,dni,date,gender,city,speciality,licence,schedule);
		//Aquí se crea una instacia de clase doctor a partir de los inputs
		
		let id = JSON.parse(localStorage.getItem('accountsDoctors')) || [];
		id = "d" + id.length;
		let tipo = "Doctor";
		this.createCuenta(d , id,tipo);
	}

	
	createCuenta(dataUser, uniqueid,tipo){
		console.log("prueba");

		if(tipo == 'Doctor'){
			let ussersDr = JSON.parse(localStorage.getItem('accountsDoctors')) || [];
			let ussertemp;
			ussertemp = ussersDr.find(function (element){
				return element.mail == document.getElementById('inputEmail1').value;
			});
			if(ussertemp){
				console.log(" email in use");
				swal({text: "Email already in use, try with another",  icon: "warning",  button: "Ok"});
				event.preventDefault();
				return;
			}
		}else{
			let ussersDr = JSON.parse(localStorage.getItem('accountsPatients')) || [];
			let ussertemp;
			ussertemp = ussersDr.find(function (element){
				return element.mail == document.getElementById('inputEmail1').value;
			});
			if(ussertemp){
				swal({text: "Email already in use, try with another",  icon: "warning",  button: "Ok"});
				event.preventDefault();
				return;
			}

		}

		let password = document.getElementById('inputPassword').value;
		let reppassword = document.getElementById('inputPasswordRepeat').value;
		if(password == ''){
			alert('Please put the password');
			event.preventDefault();
			return;
		}
		if (password == reppassword){
			let uniqueAccountNumber = uniqueid;
			let email = document.getElementById('inputEmail1').value;
			var account = new Cuenta(uniqueAccountNumber,password,email,dataUser,tipo);			
		}else{
			alert("Password doesn't match");
			event.preventDefault();
			return;

		}		
		this.saveAccounts(account,tipo);
	}

	saveAccounts(account,tipo){
		
		if(account.tipo == "Patient"){
			let recoveryAccounts = JSON.parse(localStorage.getItem('accountsPatients')) || [];
			recoveryAccounts.push(account);   
			localStorage.setItem('accountsPatients', JSON.stringify(recoveryAccounts));
			swal({text: "Thanks for join us, please check your email for gets your ID and contact administration for the last step",  icon: "success",  button: "Ok"});
			window.setTimeout(function(){}, 3000);
		}
		else{
			swal({text: "Thanks for join us, please check your email for gets your ID and contact administration for the last step",  icon: "success",  button: "Ok"});
			let recoveryAccounts = JSON.parse(localStorage.getItem('accountsDoctors')) || [];			
			recoveryAccounts.push(account);
			localStorage.setItem('accountsDoctors', JSON.stringify(recoveryAccounts));
		}
	}

//Esta funcion permite generar fechas para luego agragarlas al horario de un doctor
obtainDate(){


	let arr = [];
	let dt = new Date('2019-09-27');
	let end = new Date('2020-09-27');

	while (dt<=end) {

		dt.setDate(dt.getDate()+1);
		let year = dt.getFullYear();
		let month = ''+dt.getMonth();
		let day = ''+dt.getDate();	

		if (month.length < 2){
			month = '0' + month;
		} 

		if (day.length < 2){
			day = '0' + day;
		} 


	 let formattedDate = year + '/' + month + '/' + day;
		arr.push(formattedDate);
	}

	return arr;
}


//Esta función permite crear el horario de un doctor
createScheduleDoctor(){

	let finalSchedule = [];
	let fechas = this.obtainDate();
	let modulos = ["8:00","8:30","9:00","9:30","10:00","10:30", "11:00","11:30","12:00"];
	fechas.forEach( function(dia) {

		let horio = new Horario(dia,modulos);
		finalSchedule.push(horio);

	});

	return finalSchedule;


  }
}

