import Persona from './persona';
export default class Doctor extends Persona{

	constructor(name,lastname,dni,date,gender,city,speciality,licenseNumber,schedule){
		
		super(name,lastname,dni,date,gender,city);
		this.speciality = speciality;
		this.licenseNumber = licenseNumber;
		this.schedule = schedule;


	}
}