import '../src/style.css';
import Doctor from './doctor';
import Paciente from './paciente';
import Turno from './turno';
import moment from 'moment';
import swal from 'sweetalert';

//console.log(moment('19.07.11',moment.defaultFormat).toDate());

$(".about-focus").on("click", function(){ window.location.href = "#about-id"; $("#about-id").toggleClass("d-none");});
$("#btn-our-services").click(function(){ window.location.href = "#our-services-id"; $("#our-services-id").toggleClass("d-none");});


//CODIGO PARA MODAL LOGIN 

let logContainer = document.getElementById('loginSingup');


document.getElementById('singupbtn').addEventListener('click',function(){
	
	clickSingUp();
})

document.getElementById('loginbtn').addEventListener('click',function(){

	clickLoging();
})

function clickLoging(){

	logContainer.style.transform = 'translate(0px)';
	document.getElementById('singup').style.transform = 'translateY(-120%)';
	document.getElementById('singupbtn').setAttribute('disabled',true);
	disableButton(document.getElementById('singupbtn'));
	

}


function clickSingUp(){


	logContainer.style.transform = 'translate(102%)';
	document.getElementById('loginSingup').style.zIndex = '2';
	document.getElementById('singup').classList.remove("invisible");
	document.getElementById('singup').style.transform = 'translateY(120%)';
	document.getElementById('loginbtn').setAttribute('disabled',true);
	disableButton(document.getElementById('loginbtn'));
	
}

function disableButton(btn){

	setTimeout(function(){
		btn.removeAttribute('disabled');
	},1000);


}

// document.getElementById('inputPtEmail').addEventListener('blur',function(){

// 	let pacientes = JSON.parse(localStorage.getItem('accountsPatients')) || [];

// 	let x = pacientes.find(function(element){

// 		return element.mail == document.getElementById('inputPtEmail').value;

// 	});

// 	if(x){


// 	}else {
// 		alert('No existe un usuario con este mail');		
// 	}

// });


// MODAL LOGIN RESPONSIVE


document.getElementById('btn-responsive-modal-doctor').addEventListener('click', function(){

	clickResponsiveDoctor();
})


let docResponsive = document.getElementById('login-resoponsive-doctor');

function clickResponsiveDoctor(){

	docResponsive.style.transform = 'translate(-110%)';
	docResponsive.style.zIndex = '1';

}

document.getElementById('btn-responsive-modal-patient').addEventListener('click', function(){

	clickResponsivePtient();
});

let patResponsive = document.getElementById('login-resoponsive-patient');

function clickResponsivePtient(){

	patResponsive.style.transform = 'translate(110%)';
	patResponsive.style.zIndex = '1';

} 


document.getElementById('return-from-patient').addEventListener('click',function(){

	returnFromPatient();

});

function returnFromPatient(){

	patResponsive.style.transform = 'translate(-110%)';
	patResponsive.style.zIndex = '-1';

}

document.getElementById('return-from-doctor').addEventListener('click',function(){

	returnFromDoctor();

});

function returnFromDoctor(){

	docResponsive.style.transform = 'translate(110%)';
	docResponsive.style.zIndex = '-1';
}


// MODAL TURNOS

$('#datepicker').datepicker({todayHighlight:true});
$('#datepicker').on('changeDate', function() {
	$('#my_hidden_input').val($('#datepicker').datepicker('getFormattedDate'));
	let x = ($('#datepicker').datepicker('getFormattedDate'));
	$('#inputDate').text(moment(x).format('YYYY/MM/DD'));

	cargarHoraAgain();
});


// BUTTONS LOGIN DR AND PACIENT

var btnlogdr = document.getElementById('drLogIn').addEventListener('click',function(){

	
	clickDrLogIn(document.getElementById('inputDrEmail').value , document.getElementById('inputDrPass').value );
});
var btnlogdr1 = document.getElementById('drLogIn1').addEventListener('click',function(){

	
	clickDrLogIn(document.getElementById('inputDrEmail1').value , document.getElementById('inputDrPass1').value );
});

function clickDrLogIn(usser , pass){

	if(usser =='' || pass ==''){
		swal({text: "Email or password are incorrect",  icon: "warning", 
			buttons: { confirm: {text: "Try again",	className: "nav-care-btn"}}
		});
		event.preventDefault();
		return;
	}



	let ussersDr = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	let ussertemp;

	ussertemp = ussersDr.find(function (element){
		return element.mail == usser;
	});
	console.log(ussertemp);
	if (undefined == ussertemp) {
		document.getElementById('drLogIn').setAttribute("href","");
		localStorage.removeItem('tempUsser');
		swal({text: "Email or password are incorrect",  icon: "warning",
			buttons: { confirm: {text: "Try again",	className: "nav-care-btn"}}
		});
		event.preventDefault();
		return;
	}
	
	if(ussertemp.password == pass){

		//Preguntamos si el usuario fue dado de alta por el admin a partir de observar el estado de la variable status
		if(ussertemp.status == false){

			document.getElementById('drLogIn').setAttribute("href","");
			swal({text: "User pending for approval",  icon: "warning",
				buttons: { confirm: {text: "Ok",	className: "nav-care-btn"}}
			});
			event.preventDefault();
			return;		
		}else {
			document.getElementById('drLogIn').setAttribute("href","doctorProfile.html");
			localStorage.setItem('tempUsser',JSON.stringify(ussertemp));
			swal({text: `Welcome ${ussertemp.tipo} ${ussertemp.dataUser.name}`,  icon: "success",  button: "Ok"});
			event.preventDefault();
			console.log("se a logueado el usuario");
		}
	}else{
		document.getElementById('drLogIn').setAttribute("href","");
		localStorage.removeItem('tempUsser');
		swal({text: "Email or password are incorrect",  icon: "warning",
			buttons: { confirm: {text: "Try again",	className: "nav-care-btn"}}
		});
		event.preventDefault();
	}
}

var btnlogpt = document.getElementById('ptLogIn').addEventListener('click',function(){

	
	clickPtLogIn(document.getElementById('inputPtEmail').value , document.getElementById('inputPtPass').value );
});

var btnlogpt1 = document.getElementById('ptLogIn1').addEventListener('click',function(){

	
	clickPtLogIn(document.getElementById('inputPtEmail1').value , document.getElementById('inputPtPass1').value );
});


function clickPtLogIn(usser , pass){

	if(usser =='' || pass ==''){
		swal({text: "Please put email and password",  icon: "warning",
			buttons: { confirm: {text: "Try again",	className: "nav-care-btn"}}
		});
		event.preventDefault();
		return;
	}

	let ussersDr = JSON.parse(localStorage.getItem('accountsPatients')) || [];

	let ussertemp;

	ussertemp = ussersDr.find(function (element){
		return element.mail == usser;
	});

	// //Preguntamos si el usuario fue dado de alta por el admin a partir de observar el estado de la variable status

		if (undefined == ussertemp) {
		document.getElementById('ptLogIn').setAttribute("href","");
		localStorage.removeItem('tempUsser');
		swal({text: "Email or password are incorrect",  icon: "warning",
			buttons: { confirm: {text: "Try again",	className: "nav-care-btn"}}
		});
		event.preventDefault();
		return;
	}


	console.log(ussertemp);
	if(ussertemp.password == pass){
		if(ussertemp.status == false){
			document.getElementById('ptLogIn').setAttribute("href","");
			swal({text: "User pending for approval",  icon: "warning",
			buttons: { confirm: {text: "Ok",	className: "nav-care-btn"}}
		});
			event.preventDefault();
			return;		
		} else {
			localStorage.setItem('tempUsser',JSON.stringify(ussertemp));
			swal({text: `Welcome ${ussertemp.tipo} ${ussertemp.dataUser.name}`,  icon: "success",  button: "Ok"});
			event.preventDefault();
			console.log("se a logueado el usuario");
		}
	}else{
		localStorage.removeItem('tempUsser');
		swal({text: "Email or password are incorrect",  icon: "warning",
			buttons: { confirm: {text: "Try again",	className: "nav-care-btn"}}
		});
		event.preventDefault();
	}

}



//---------LLERNAR DESPLEGABLE DE MEDICOS EN EL MODAL DE JJ--------

function fillDoctors(){

	let desplegable = document.getElementById('inputState');

	let doctors = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	desplegable.innerHTML += `<option>Choose a doctor </option> `;

	doctors.forEach( function(element, index) {
		
		desplegable.innerHTML += `<option id="${element.numeroDeCuenta}">${element.dataUser.name} ${element.dataUser.lastname} (${element.dataUser.speciality})</option> `;
	});
}

fillDoctors();

//---------LLERNAR DESPLEGABLE DE MEDICOS EN EL MODAL DE JJ--------


//----------LLENAR LAS HORAS DE ATENCIÓN EN EL MODAL DE JJ----------------


function llenarHorasAtención(){

	let doctors = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	let doctorsDesplegable = document.getElementById('inputState');

	doctorsDesplegable.addEventListener('change',function(){

		let getDoctor = document.getElementById('inputState');

		let doc = doctors.find(function(element){

			return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);


		});



		let form = document.getElementById('hoursAtention');
		form.innerHTML ='';

		let fullScheduleDoc = [];
		fullScheduleDoc = doc.dataUser.schedule;


		let oneDayDoc = fullScheduleDoc.find(function(element){

			return element.fecha == document.getElementById('inputDate').textContent;		


		});



		oneDayDoc.hora.forEach( function(element, index) {

			form.innerHTML += ` <label id="btnhour${index}" class="btn btn-outline-success m-1">
			<input type="radio" name="options" id="option${index}" value="${element}" autocomplete="off" checked>${element}
			</label>`	

			setTimeout(function(){

				let btn = document.getElementById(`btnhour${index}`);

				btn.addEventListener('click',function(){


					document.getElementById('hourSelected').textContent = document.getElementById(`option${index}`).value;

				});


			},0);

		});

	})

}

llenarHorasAtención();

//----------LLENAR LAS HORAS DE ATENCIÓN EN EL MODAL DE JJ----------------


//-------------CREAMOS UN TURNO----------------


function iniciarCreacionTurno(){

	let btnturn = document.getElementById('btnConfirmTurno');

	btnturn.addEventListener('click',function(){



		let use = JSON.parse(localStorage.getItem('tempUsser'));

		if(use != undefined){

			let turno = crearTurno();

			let getDoctor = document.getElementById('inputState');

			let doc = doctors.find(function(element){

				return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);


			});
		}
		else{
			alert('Debe loguearse');
			swal("Please put email and password");
			event.preventDefault();
		}
	})
}

iniciarCreacionTurno();

function crearTurno(){


	let fecha = document.getElementById('inputDate').textContent;
	let hora =  document.getElementById('hourSelected').textContent;
	let descripcion = document.getElementById('reasonsTextarea').value;

	let doctors = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	let doctorsDesplegable = document.getElementById('inputState');
	let getDoctor = document.getElementById('inputState');
	let userPatientLog = JSON.parse(localStorage.getItem('tempUsser')) || [];

	let doc = doctors.find(function(element){

		return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);
		

	});

	
	
	if(fecha=="" || hora == "" || descripcion == ""){
		alert('Fields empty');

	}
	else{

		if(fecha<moment().format('MM/DD/YYYY')){
			alert('La fecha elegida ya pasó');
			return;
		}

		let turno = new Turno(fecha,hora,descripcion);
		turno.idDoctor = doc.numeroDeCuenta;
		turno.idPatient = userPatientLog.numeroDeCuenta;
		let turnosContainer = JSON.parse(localStorage.getItem('turnos')) || [];
		turnosContainer.push(turno);
		localStorage.setItem('turnos', JSON.stringify(turnosContainer));

		borrarHoraTurno(hora,fecha); //lamamos a esta función para borrar el día que se acaba de usar en el turno	

		alert('An appointment was created succesfully');

		return ;	

	}

}
//-------------CREAMOS UN TURNO----------------



// ------------FUNCIONES UTILIZADAS EN CREAR TURNO-----------
function borrarHoraTurno(hora,fecha){

	//obtengo un doctor
	let getDoctor = document.getElementById('inputState');


	let doctores = JSON.parse(localStorage.getItem('accountsDoctors')) || [];


	let doc = doctores.find(function(element){

		return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);
		

	});

	//obtengo el índice de esa fecha

	//obtengo el indice del doc
	let idDoc = doctores.findIndex(function(element){ return element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id});

	//obtengo el indice de un elemento de la colección schedule
	let idFech = doctores[idDoc].dataUser.schedule.findIndex(function(element){return element.fecha == fecha });

	//obtengo el indice de un elemento de la colección hora
	let idhora = doctores[idDoc].dataUser.schedule[idFech].hora.findIndex(function(element){return element == hora});

	//borro esa hora de la coleción 
	doctores[idDoc].dataUser.schedule[idFech].hora.splice(idhora,1);

	//guardo en localStorage
	localStorage.setItem('accountsDoctors', JSON.stringify(doctores));

	//VOLVEMOS A LLENAR EL DESPLEGAB

	cargarHoraAgain();

}


function cargarHoraAgain(){


	let doct = JSON.parse(localStorage.getItem('accountsDoctors')) || [];

	// console.log('losDocsson: ')
	// console.log(doctores);

	let getDoctor = document.getElementById('inputState');

	let doc = doct.find(function(element){

		return ( element.numeroDeCuenta == getDoctor[getDoctor.selectedIndex].id);


	});



	let form = document.getElementById('hoursAtention');
	form.innerHTML ='';

	let fullScheduleDoc = [];
	fullScheduleDoc = doc.dataUser.schedule;


	let oneDayDoc = fullScheduleDoc.find(function(element){

		return element.fecha == document.getElementById('inputDate').textContent;		


	});



	oneDayDoc.hora.forEach( function(element, index) {

		form.innerHTML += ` <label id="btnhour${index}" class="btn btn-outline-success m-1">
		<input type="radio" name="options" id="option${index}" value="${element}" autocomplete="off" checked>${element}
		</label>`	

		setTimeout(function(){

			let btn = document.getElementById(`btnhour${index}`);

			btn.addEventListener('click',function(){


				document.getElementById('hourSelected').textContent = document.getElementById(`option${index}`).value;

			});


		},0);

	});


}

// ------------FUNCIONES UTILIZADAS EN CREAR TURNO-----------

//-------Cargar el logo de usuario registrado-------

function agregarLogodDeLogueo(){

	let log = document.getElementById('logo-log');

	let use = JSON.parse(localStorage.getItem('tempUsser'));

	if(use != undefined){


		
		if(use.tipo == "Patient"){

			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="patientProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1"href="#">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2" class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="patientProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}

		if(use.tipo == "Doctor"){


			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="doctorProfile.html">Profile</a>
			<a class="dropdown-item" id="deslog1" href="#">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a id="deslog2"class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="doctorProfile.html" style="background-color: #227a66">
				Profile
				</a>`);


		}


	}
	else{
		console.log(use);
		log.innerHTML = `<a class="nav-link btn nav-care-btn pb-1" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
		Login/Register
		</a>`;
	}

	deslogueo();


}

//DESLOGUEO POR F.GRAMAJO
function deslogueo(){

	let d1 = document.getElementById('deslog1');
	let d2 = document.getElementById('deslog2');

	if(d1 != undefined){
		d1.addEventListener('click',function(){
	
			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}

	if(d2 != undefined){
		d2.addEventListener('click',function(){

			localStorage.removeItem('tempUsser');
			location.reload();
		});
	}
}



function agregarEventosBotonesLogoLogueo(){

	agregarLogodDeLogueo();

	let botP = document.getElementById('ptLogIn');

	botP.addEventListener('click',function(){

		agregarLogodDeLogueo();

	});


	// Botones en responsive del modal al que se agregan eventos para cambiar el boton de logueo

	let botPr = document.getElementById('drLogIn1');

	botPr.addEventListener('click',function(){

		agregarLogodDeLogueo();

	});

	let botPp = document.getElementById('ptLogIn1');

	botPp.addEventListener('click',function(){

		agregarLogodDeLogueo();

	});
}



agregarEventosBotonesLogoLogueo();

document.getElementById("btn-close-modal").addEventListener("click", function() {
	document.getElementById("form").reset()});
document.getElementById("btn-close-modal1").addEventListener("click", function() {
	document.getElementById("form1").reset()});

