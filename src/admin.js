import '../src/style.css';
import '../src/admin.css';

function getAllDr(){
	return JSON.parse(localStorage.getItem('accountsDoctors')) || [];
}
function getAllPt(){
	return JSON.parse(localStorage.getItem('accountsPatients')) || [];
}

function put(){
	let drTable = document.getElementById('drTable');
	let ptTable = document.getElementById('ptTable');
	drTable.innerHTML='';
	ptTable.innerHTML='';
	let drList = getAllDr();
	let ptList = getAllPt();
	let i = 0;
	let btnDr =[];
	let btnDr1 =[];
	drList.forEach(function(element, index){


		drTable.innerHTML+=	`<tr>
							    <th scope="row">${element.numeroDeCuenta}</th>
							    <td>${element.dataUser.name} ${element.dataUser.lastname}</td>
							    <td>${element.status}</td>
							    <td><button class="btn bg-success" id="btnDrOn-${index}">ON</button>
				      				<button class="btn bg-danger" id="btnDrOff-${index}">OFF</button></td>
							    <td><button class="btn bg-primary" id="btnDrEdit-${index}">Edit</button></td>
								</tr>
					   		<tr>`;


					  setTimeout(function(){

					let btn1 = document.getElementById(`btnDrOn-${index}`);
					btn1.addEventListener('click', function(){
					
					highLogic(index);
					});
				
					
					let btn2= document.getElementById(`btnDrOff-${index}`);
					btn2.addEventListener('click', function(){
					notNighLogic(index);
					});
				
			}, 0);

	})
	ptList.forEach(function(element, index){

		ptTable.innerHTML+=	`<tr>
							    <th scope="row">${element.numeroDeCuenta}</th>
							    <td>${element.dataUser.name} ${element.dataUser.lastname}</td>
							    <td>${element.status}</td>
							    <td><button class="btn bg-success" id="btnPtOn-${index}">ON</button>
				      				<button class="btn bg-danger" id="btnPtOff-${index}">OFF</button></td>
							    <td><button class="btn bg-primary" id="btnPtEdit-${index}">Edit</button></td>
								</tr>
					   		<tr>`;

		setTimeout(function(){

					let btn5 =document.getElementById(`btnPtOn-${index}`);
					btn5.addEventListener('click', function(){
					
					highLogicPt(index);
					});
				
					
					let btn6= document.getElementById(`btnPtOff-${index}`);
					btn6.addEventListener('click', function(){
					notNighLogicPt(index);
					});
				
		}, 0);

	});


}

put();

function highLogic(c){

	let drList = getAllDr();
	console.log(c);
	drList[c].status = true;
		
	localStorage.setItem('accountsDoctors', JSON.stringify(drList));
	put();
}


function notNighLogic(c){
	let drList = getAllDr();
	drList[c].status = false;	
	console.log(c);
	localStorage.setItem('accountsDoctors', JSON.stringify(drList));
	put();
}

function highLogicPt(c){

	let ptList = getAllPt();
	ptList[c].status = true;		
	localStorage.setItem('accountsPatients', JSON.stringify(ptList));
	put();
}


function notNighLogicPt(c){
	let ptList = getAllPt();
	ptList[c].status = false;	
	localStorage.setItem('accountsPatients', JSON.stringify(ptList));
	put();
}
