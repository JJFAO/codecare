import '../src/donacionesStyle.css';

function agregarLogodDeLogueo(){

	let log = document.getElementById('logo-log');

	let use = JSON.parse(localStorage.getItem('tempUsser'));

	if(use != undefined){


		
		if(use.tipo == "Patient"){

			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="patientProfile.html">Profile</a>
			<a class="dropdown-item" id=""href="#">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="patientProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}

		if(use.tipo == "Doctor"){


			log.classList.add('d-none');
			log.classList.add('d-md-block');
			log.innerHTML = `<a class="btn dropdown-toggle-modificado" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
			<i class="far fa-user fa-2x"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-modificado dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" id="" href="doctorProfile.html">Profile</a>
			<a class="dropdown-item" id=""href="#">Logout</a>
			</div>`;

			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="" style="background-color: #227a66">
				Logout
				</a>`);
			log.insertAdjacentHTML('afterend',`<a class="nav-link btn nav-care-btn pb-1 d-sm-block d-md-none" href="doctorProfile.html" style="background-color: #227a66">
				Profile
				</a>`);
		}


	}
	else{
		console.log(use);
		log.innerHTML = `<a class="nav-link btn nav-care-btn pb-1" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
		Login/Register
		</a>`;
	}


}

agregarLogodDeLogueo();