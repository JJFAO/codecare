# 🚀 Welcome to your new awesome project!

This project has been created using **webpack scaffold**, you can now run

```
npm run build
```

or

```
yarn build
```

to bundle your application

Se agregó la carpeta .gitignored para que to lo que se encuentre dentro de él sea ignorado a al hora de hacer git push/commit.

Usar "npm install" cada vez se haga un pull para poder hacer las pruebas correspondientes